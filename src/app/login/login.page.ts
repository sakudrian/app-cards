import { Component, OnInit,} from '@angular/core';
import { DataServiceService } from '../services/data-service.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  response: Observable<any>;
  client : any;
  constructor(private dataService : DataServiceService,private router: Router) { 
  	this.client = {
  		email:"",
  		password:""
  	};
  }

  ngOnInit() {
  }

  makeLogin(){
  	console.log("el cliente",this.client);
  	this.dataService.login("clients", "login", this.client).subscribe(
        result => {
        	console.log("RESPUesta",result);
        	this.router.navigate(['/home'])
      },
      error=>{
      	console.log("erro",error);
      });
  }

  redirect(){
  	this.router.navigate(['/register']);
  }

}
