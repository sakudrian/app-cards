import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../services/data-service.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  newClient: any;
  constructor(private dataService : DataServiceService,private router: Router) { 
  	this.newClient = {
  		name:"",
  		email:"",
  		password:""
  	}
  }

  ngOnInit() {
  }

  makeRegister(){

  	this.dataService.registro("clients", this.newClient).subscribe(
        result => {
        	alert("Exito!");
        	this.router.navigate(['/login'])
      },
      error=>{
      	console.log("erro",error);
      });
  }

}
