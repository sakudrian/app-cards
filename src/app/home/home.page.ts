import { Component,OnInit } from '@angular/core';
import { DataServiceService } from '../services/data-service.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  response: Observable<any>;
  news: any;
  constructor(private dataService : DataServiceService) {
  	console.log("home");
  	this.news={};
  }

  ngOnInit() {
  	this.dataService.getNews().subscribe(
  		resp=>{
  			console.log("NEWS",resp)
  			this.news = resp;
  		}
  	);
  }


}
