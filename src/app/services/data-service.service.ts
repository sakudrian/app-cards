import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class DataServiceService {
	url = 'http://localhost:3000/api/';
  	apiKey = '3a6763cea0504638a716dd7a6b339950'; // <-- Enter your own key here!

  constructor(private http: HttpClient) { 

  }

  	login(model: string, path: string, credentials: any) {
		let jsonContent = JSON.stringify(credentials);
		let params = jsonContent;
		// console.log("PARAMETROS",params)
		// console.log(this.url + model + '/' + type + params);
		return this.http.post(this.url + model + '/' + path, params, 
			{ headers: httpOptions.headers}).pipe(map(res => res));
		
	}

	registro(model: string, credentials: any){
		let jsonContent = JSON.stringify(credentials);
		let params = jsonContent;

		return this.http.post(this.url + model,params, 
			{ headers: httpOptions.headers}).pipe(map(res => res));
	}

	getNews(){		let url="https://newsapi.org/v2/top-headlines?country=us&apiKey="+this.apiKey;
		return this.http.get(url).pipe(map(res => res));
	}
}
